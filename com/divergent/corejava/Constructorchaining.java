package com.divergent.corejava;

/**
 * @author Amit kumar singh chauhan
 *
 */

public class Constructorchaining {
	public static class Employee
	{   
	    public String empName;
	    public int empSalary;
	    public String address;

	    //default constructor of the class
	    public Employee()
	    {
	    	//the constructor call with String par
	        this("rohit");
	    }

	    public Employee(String name)
	    {
	    	//call the constructor with (String, int) par
	    	this(name, 89000);
	    }
	    public Employee(String name, int sal)
	    {
	    	//call the constructor with (String, int, String) par
	    	this(name, sal, "gwalior");
	    }
	    public Employee(String name, int sal, String addr)
	    {
	    	this.empName=name;
	    	this.empSalary=sal;
	    	this.address=addr;
	    }

	    void disp() {
	    	System.out.println("Employee Name: "+empName);
	    	System.out.println("Employee Salary: "+empSalary);
	    	System.out.println("Employee Address: "+address);
	    }
	    public static void main(String[] args)
	    {
	        Employee obj = new Employee();
	        obj.disp();
	    }
	}
}

